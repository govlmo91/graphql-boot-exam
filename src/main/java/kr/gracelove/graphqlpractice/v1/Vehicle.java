package kr.gracelove.graphqlpractice.v1;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@EqualsAndHashCode
public class Vehicle {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String type;

    private String modelCode;

    private String brandName;

    private LocalDate launchDate;

    @Transient
    private String formattedDate;
}
